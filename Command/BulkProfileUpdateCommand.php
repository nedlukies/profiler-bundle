<?php

namespace Madforit\ProfilerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ODM\MongoDB\DocumentManager;


class BulkProfileUpdateCommand extends ContainerAwareCommand {
    
    /**
     *
     * @var DocumentManager
     */
    private $dm;
    
    protected function configure() {
        $this
            ->setName('madforit:profiler:profile:update')
            ->setDescription('Update Profile Attributes')
            ->addOption('key', 'k', InputOption::VALUE_REQUIRED, 'Key to update')
            ->addOption('file', 'f', InputOption::VALUE_REQUIRED, 'File cotaining keys to update')
            ->addOption('data', 'd', InputOption::VALUE_REQUIRED, 'Data File');
           
    }
    
    protected function execute(InputInterface $input, OutputInterface $output) {
        
        gc_enable();
        
        $this->dm = $this->getContainer()->get('doctrine_mongodb')->getManager();
        
        
        $this->output = $output;
        
        if (!$input->getOption('key') && !$input->getOption('file')) {
            $output->writeln('A key or file is required');
            exit();
        }
        
        if (!$input->getOption('data')) {
            $output->writeln('Data file is required');
            exit();
        } elseif (!is_file($input->getOption('data'))) {
            $output->writeln('Data file is is not readable');
            exit();
        }
        
        $data = json_decode(file_get_contents($input->getOption('data')), true);
        
        if (!$data) {
            $output->writeln('Data file is invalid');
            exit();
        }
        
        foreach ($data as $attribute_name => $value) {
            
            if ($input->getOption('key')) {
                $this->updateProfile($input->getOption('key'), $attribute_name, $value);
            } else {
                if (!is_file($input->getOption('file'))) {
                    $output->writeln("Key file is invalid");
                    exit();
                }
                
                $fd = fopen($input->getOption('file'),'r');
                $i = 0;
                
                while ($key = fgetcsv($fd)) {
                   $output->writeln($i);
                   $i++;
                   $this->updateProfile($key[0], $attribute_name, $value);
                   $this->dm->clear();
                   gc_collect_cycles();
                }
                
                fclose($fd);
            }
        }
        
    }
    
    private function updateProfile($key, $attribute_name, $value) {
        $this->output->writeln(sprintf("Updating %s with attribute %s data %s",$key, $attribute_name, json_encode($value)));
        
        $result = $this->getContainer()->get('madforit.profiler')->updateProfileAttribute($key, $attribute_name, $value);
        
        if ($result) {
            return true;
        }
        
    }
}
