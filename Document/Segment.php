<?php

namespace Madforit\ProfilerBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;


/**
 * @MongoDB\Document
 * @MongoDB\HasLifecycleCallbacks
 */

class Segment
{
    /**
     * @MongoDB\Id
     */
    protected $id;
    
    /**
     * @MongoDB\String
     */
    
    protected $name;
    
    /**
     * @MongoDB\Int
     */
    
    protected $count;
    
    /**
     * @MongoDB\EmbedMany(targetDocument="Criterion")
     */
    protected $criteria = array();
    
    
    /**
     * #MongoDB\ReferenceMany(targetDocument="Profile")
     */
    
    protected $profiles = array();
    
    /**
     * @MongoDB\Date
     */
    
    protected $created;
    
    /**
     * @MongoDB\Date;
     */
    
    protected $updated;
    
    public function __construct()
    {
        $this->criteria = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    
    /**
     * @MongoDB\PrePersist
     */
    public function setCreatedDate() {
        $this->setCreated(new \DateTime());
        $this->setUpdated($this->getCreated());
    }
    /**
     * @MongoDB\PreUpdate
     */
    
    public function setUpdatedDate() {
        $this->setUpdated(new \DateTime());
    }
    
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set count
     *
     * @param int $count
     * @return self
     */
    public function setCount($count)
    {
        $this->count = $count;
        return $this;
    }

    /**
     * Get count
     *
     * @return int $count
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Add criterium
     *
     * @param Madforit\ProfilerBundle\Document\Criterion $criterium
     */
    public function addCriterion(\Madforit\ProfilerBundle\Document\Criterion $criterium)
    {
        $this->criteria[] = $criterium;
    }

    /**
     * Remove criterium
     *
     * @param Madforit\ProfilerBundle\Document\Criterion $criterium
     */
    public function removeCriterion(\Madforit\ProfilerBundle\Document\Criterion $criterium)
    {
        $this->criteria->removeElement($criterium);
    }

    /**
     * Get criteria
     *
     * @return Doctrine\Common\Collections\Collection $criteria
     */
    public function getCriteria()
    {
        return $this->criteria;
    }

    /**
     * Set created
     *
     * @param date $created
     * @return self
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * Get created
     *
     * @return date $created
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param date $updated
     * @return self
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * Get updated
     *
     * @return date $updated
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
