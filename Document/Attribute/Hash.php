<?php

namespace Madforit\ProfilerBundle\Document\Attribute;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

use Madforit\ProfilerBundle\Document\Attribute;

/**
 * @MongoDB\EmbeddedDocument
 */
class Hash
{
    /**
     * @MongoDB\Id
     */
    protected $id;
    
    /**
     * @MongoDB\Hash
     */    
    protected $data;
    
    /**
     * @MongoDB\ReferenceOne(targetDocument="\Madforit\ProfilerBundle\Document\Attribute")
     */
    protected $attribute;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set data
     *
     * @param hash $data
     * @return self
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * Get data
     *
     * @return hash $data
     */
    public function getData()
    {
        return $this->data;
    }

        /**
     * Set attribute
     *
     * @param Madforit\ProfilerBundle\Document\Attribute $attribute
     * @return self
     */
    public function setAttribute(\Madforit\ProfilerBundle\Document\Attribute $attribute)
    {
        $this->attribute = $attribute;
        return $this;
    }

    /**
     * Get attribute
     *
     * @return Madforit\ProfilerBundle\Document\Attribute $attribute
     */
    public function getAttribute()
    {
        return $this->attribute;
    }
}
