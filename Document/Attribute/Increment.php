<?php

namespace Madforit\ProfilerBundle\Document\Attribute;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;


/**
 * @MongoDB\EmbeddedDocument
 */
class Increment
{   
    /**
     * @MongoDB\Id
     */
    protected $id;
    
    /**
     * @MongoDB\Increment
     */    
    protected $increment;
    
    /**
     * @MongoDB\ReferenceOne(targetDocument="\Madforit\ProfilerBundle\Document\Attribute")
     */
    protected $attribute;

    /**
     * Set increment
     *
     * @param increment $increment
     * @return self
     */
    public function setIncrement($increment)
    {
        $this->increment = $increment;
        return $this;
    }
    
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get increment
     *
     * @return increment $increment
     */
    public function getIncrement()
    {
        return $this->increment;
    }

    /**
     * Set attribute
     *
     * @param Madforit\ProfilerBundle\Document\Attribute $attribute
     * @return self
     */
    public function setAttribute(\Madforit\ProfilerBundle\Document\Attribute $attribute)
    {
        $this->attribute = $attribute;
        return $this;
    }

    /**
     * Get attribute
     *
     * @return Madforit\ProfilerBundle\Document\Attribute $attribute
     */
    public function getAttribute()
    {
        return $this->attribute;
    }
}
