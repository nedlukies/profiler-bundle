<?php

namespace Madforit\ProfilerBundle\Document\Attribute;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\EmbeddedDocument
 */
class Date
{
    /**
     * @MongoDB\Id
     */
    protected $id;
    
    /**
     * @MongoDB\Date
     * @MongoDB\Index
     */    
    protected $date;
    
    /**
     * @MongoDB\ReferenceOne(targetDocument="\Madforit\ProfilerBundle\Document\Attribute")
     */
    protected $attribute;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set data
     *
     * @param DateTime($date)
     * @return self
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * Get data
     *
     * @return DateTime $date
     */
    public function getDate()
    {
        return $this->date;
    }

        /**
     * Set attribute
     *
     * @param Madforit\ProfilerBundle\Document\Attribute $attribute
     * @return self
     */
    public function setAttribute(\Madforit\ProfilerBundle\Document\Attribute $attribute)
    {
        $this->attribute = $attribute;
        return $this;
    }

    /**
     * Get attribute
     *
     * @return Madforit\ProfilerBundle\Document\Attribute $attribute
     */
    public function getAttribute()
    {
        return $this->attribute;
    }
}
