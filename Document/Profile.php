<?php

namespace Madforit\ProfilerBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;


/**
 * @MongoDB\Document
 * @MongoDB\HasLifecycleCallbacks
 */
class Profile
{
    /**
     * @MongoDB\Id
     */
    protected $id;
    
    /**
     * 
     * @MongoDB\String
     * @MongoDB\Index(unique=true, order="asc")
     */    
    protected $key;
    
    /**
     * @MongoDB\EmbedMany(
     *  discriminatorMap={
     *      "hash"="Madforit\ProfilerBundle\Document\Attribute\Hash",
     *      "increment"="Madforit\ProfilerBundle\Document\Attribute\Increment",
     *      "date"="Madforit\ProfilerBundle\Document\Attribute\Date"
     *  }
     * )
     * @MongoDB\Index
     */
    
    protected $attributes = array();
    
    /**
     * @MongoDB\Date
     */
    
    protected $created;
    
    /**
     * @MongoDB\Date;
     */
    
    protected $updated;

    public function __construct()
    {
        $this->attributes = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    
    /**
     * @MongoDB\PrePersist
     */
    public function prePersist()
    {
        $this->setCreated(new \DateTime);
        $this->setUpdated($this->getCreated());
    }
    
    /** 
     * @MongoDB\PreUpdate
     */
    public function preUpdate()
    {
        $this->setUpdated(new \DateTime());
    }
    
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set key
     *
     * @param string $key
     * @return self
     */
    public function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     * Get key
     *
     * @return string $key
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Add attribute
     *
     * @param $attribute
     */
    public function addAttribute($attribute)
    {
        $this->attributes[] = $attribute;
    }

    /**
     * Remove attribute
     *
     * @param $attribute
     */
    public function removeAttribute($attribute)
    {
        $this->attributes->removeElement($attribute);
    }

    /**
     * Get attributes
     *
     * @return Doctrine\Common\Collections\Collection $attributes
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Set created
     *
     * @param date $created
     * @return self
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * Get created
     *
     * @return date $created
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param date $updated
     * @return self
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * Get updated
     *
     * @return date $updated
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
