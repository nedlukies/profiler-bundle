<?php

namespace Madforit\ProfilerBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;


/**
 * @MongoDB\EmbeddedDocument
 */
class Criterion
{
    
    /**
     * @MongoDB\ReferenceOne(targetDocument="Attribute")
     */
    protected $attribute;
    
    /**
     * @MongoDB\String
     */
    
    protected $name;
    
    /**
     * @MongoDB\String
     */
    
    protected $rule;
    
    /**
     * @MongoDB\String
     */
    
    protected $value;

    /**
     * Set attribute
     *
     * @param Madforit\ProfilerBundle\Document\Attribute $attribute
     * @return self
     */
    public function setAttribute(\Madforit\ProfilerBundle\Document\Attribute $attribute)
    {
        $this->attribute = $attribute;
        return $this;
    }

    /**
     * Get attribute
     *
     * @return Madforit\ProfilerBundle\Document\Attribute $attribute
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * Set rule
     *
     * @param string $rule
     * @return self
     */
    public function setRule($rule)
    {
        $this->rule = $rule;
        return $this;
    }

    /**
     * Get rule
     *
     * @return string $rule
     */
    public function getRule()
    {
        return $this->rule;
    }
    
    /**
     * Set name
     *
     * @param string $names
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value
     *
     * @param string $values
     * @return self
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * Get value
     *
     * @return string $value
     */
    public function getValue()
    {
        return $this->value;
    }
}
