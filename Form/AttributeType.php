<?php

namespace Madforit\ProfilerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Madforit\ProfilerBundle\Form\CriterionType;

class AttributeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        
        $ls = scandir(__DIR__ . '/../Document/Attribute');
                
        $types = array();

        foreach ($ls as $file) {
            if (preg_match("/\.php$/", $file)) {
                $name = preg_replace("/(.*)\.php$/","\\1", $file);
                $types[$name] = $name;
            }
        }
                
                
        $builder->add('name');
        $builder->add('type','choice',array(
            'choices' => $types
        ));
        
    }
    
        public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Madforit\ProfilerBundle\Document\Attribute',
            'render_fieldset' => false,
            'show_legend' => false,
        ));
    }

    public function getName()
    {
        return 'madforit_profiler_attribute';
    }
    
}