<?php

namespace Madforit\ProfilerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CriterionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('attribute','document',array(
            'class' => 'Madforit\ProfilerBundle\Document\Attribute',
            'property' => 'name',
            'horizontal_input_wrapper_class' => 'col-lg-4',
        ));
        
        $builder->add('name','text');
        $builder->add('rule','choice',array(
            'choices' => array(
                'eq' => '=',
                'neq' => '!=',
                'lt' => '<',
                'lte' => '<=',
                'gt' => '>',
                'gte' => '>=',
                'in' => "IN (x,y,z)",
                'notin' => "NOT IN (x,y,z)",
                'contains' => 'contains x'
            )
            
        ));
        
        $builder->add('value','text');
        
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Madforit\ProfilerBundle\Document\Criterion',
            
        ));
    }

    public function getName()
    {
        return 'madforit_profiler_criterion';
    }
}
    