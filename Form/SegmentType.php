<?php

namespace Madforit\ProfilerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Madforit\ProfilerBundle\Form\CriterionType;

class SegmentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name');
        $builder->add('criteria', 'collection', array(
            'type' => new CriterionType(),
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'prototype' => true,
            'show_legend' => false,
            'options' => array(
                'label_render' => false,
                
            )
        ));
        
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Madforit\ProfilerBundle\Document\Segment',
            'render_fieldset' => false,
            'show_legend' => false,
        ));
    }

    public function getName()
    {
        return 'madforit_profiler_segment';
    }
    
}