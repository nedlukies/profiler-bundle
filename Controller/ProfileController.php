<?php

namespace Madforit\ProfilerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use Madforit\ProfilerBundle\Document\Profile;

/**
 * @Route("/profile")
 */
class ProfileController extends Controller
{
    /**
     * @Route("/")
     * @Template()
     * @Method({"GET"})
     */
    public function indexAction()
    {
        return array();
    }
    
    /**
     * @Route("/_list")
     * @Method({"GET"})
     */
    public function listAction(Request $request)
    {
        $params = $request->query->all();
        
        
        $dm = $this->get('doctrine_mongodb')->getManager();
        
        $output['iTotalRecords'] = $dm->createQueryBuilder('MadforitProfilerBundle:Profile')
                ->count()
                ->getQuery()
                ->execute();
        
        $output['sEcho'] = $params['sEcho'];
        $output['iTotalDisplayRecords'] = $output['iTotalRecords'];
        
        $data = $dm->createQueryBuilder('MadforitProfilerBundle:Profile')
                ->skip($params['iDisplayStart'])
                ->limit($params['iDisplayLength'])
                ->getQuery()
                ->execute();
        
        foreach ($data as $row) {
            $output['aaData'][] = array(
                $row->getId(),
                $row->getKey(),
                $row->getCreated()->format('Y-m-d H:i:s'),
                $row->getUpdated()->format('Y-m-d H:i:s')  
            );
                    
        }
        
        return new JsonResponse($output);
 
    }
    
    /**
     * @Route("/test")
     */
    public function testAction() 
    {
        
        $profiler = $this->get('madforit.profiler');
        
        $dm = $this->get('doctrine_mongodb')->getManager();
        
        $segment = $dm->getRepository('MadforitProfilerBundle:Segment')->findOneById('534f240c89c7c2efcfb7acd9');
        
        \Doctrine\Common\Util\Debug::dump($profiler->matchingProfiles($segment));
        die();
        
    }
    
    /**
     * @Route("/view/{profile_id}")
     * @Template()
     */
    
    public function viewAction(Request $request, $profile_id) {
        return array();
    }
}
