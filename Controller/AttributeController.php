<?php

namespace Madforit\ProfilerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;


use Madforit\ProfilerBundle\Form\AttributeType;
use Madforit\ProfilerBundle\Document\Attribute;

/**
 * @Route("/profile/attribute")
 */
class AttributeController extends Controller
{
    /**
     * @Route("/")
     * @Template()
     * @Method({"GET"})
     */
    public function indexAction()
    {
        $dm = $this->get('doctrine_mongodb')->getManager();
        
        return array('attributes' => $dm->getRepository('MadforitProfilerBundle:Attribute')->findAll());
    }
    
    /**
     * @Route("/add")
     * @Template()
     * @Method({"GET","POST"})
     */

    public function addAction(Request $request)
    {
         $dm = $this->get('doctrine_mongodb')->getManager();
         
         $attribute = new Attribute;
         
         $form = $this->createForm(new AttributeType(), $attribute);
         
         if ($request->isMethod('POST')) {
             $form->handleRequest($request);
             
             $flash = $this->get('session')->getFlashBag();
             
             if ($form->isValid()) {
                 
                 $dm->persist($attribute);
                 
                 $dm->flush();
                 
                 
                 $flash->add('success','The attribute was created');
                 return $this->redirect($this->generateUrl('madforit_profiler_attribute_index'));
             } else {
                 $flash->add('warning', 'The attribute could not be created');
             }
         }
         
         return array('form'=>$form->createView(), 'attribute' => $attribute);
    }
    
    /**
     * @Route("/edit/{attribute_id}")
     * @Template()
     * @Method({"GET","POST"})
     */

    public function editAction(Request $request, $attribute_id)
    {
         $dm = $this->get('doctrine_mongodb')->getManager();
         
         $attribute = $dm->getRepository('MadforitProfilerBundle:Attribute')->find($attribute_id);
         
         if (!$attribute) {
             throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Attribute Not found');
             
         }
         
         $form = $this->createForm(new AttributeType(), $attribute);
         
         if ($request->isMethod('POST')) {
             $form->handleRequest($request);
             
             $flash = $this->get('session')->getFlashBag();
             
             if ($form->isValid()) {
                 
                 $dm->persist($attribute);
                 
                 $dm->flush();
                 
                 
                 $flash->add('success','The attribute was updated');
                 return $this->redirect($this->generateUrl('madforit_profiler_attribute_index'));
             } else {
                 $flash->add('warning', 'The attribute could not be updated');
             }
         }
         
         return array('form'=>$form->createView(), 'attribute' => $attribute);
    }
    
}