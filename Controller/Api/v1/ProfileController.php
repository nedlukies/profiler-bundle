<?php

namespace Madforit\ProfilerBundle\Controller\Api\v1;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

use Madforit\ProfilerBundle\Document\Profile;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/api/v1/profile")
 */
class ProfileController extends Controller
{
    
    /**
     * @Route("/{key}")
     * @Rest\View();
     */
    public function getAction($key)
    {
        $dm = $this->get('doctrine_mongodb')->getManager();
        
        $profile = $dm->getRepository('MadforitProfilerBundle:Profile')->findOneBy(array('key' => $key));
        
        if (!$profile) {
            throw new NotFoundHttpException('Profile not found');
        }
        
        return $profile;
    }
    
    /**
     * @Route("/{key}/attribute/{attribute_name}")
     * @Rest\View();
     * @Method({"GET"})
     */
    public function getAttributeAction($key, $attribute_name)
    {
        
        $profile = $this->getAction($key);
        
        foreach ($profile->getAttributes() as $attribute) {
            
            if ($attribute->getAttribute()->getName() == $attribute_name) {
            
                switch ($attribute->getAttribute()->getType()) {
                    case 'Hash':
                        return $attribute->getData();
                        break;
                    case 'Date':
                        return array('ISO8601' => $attribute->getDate()->format(\DateTime::ISO8601));
                        
                }
           
                
            }
        }
         
    }
    
    /**
     * @Route("/{key}/attribute/{attribute_name}")
     * @Rest\View();
     * @Method({"PUT","POST"})
     */
    
    public function putAttributeAction(Request $request, $key, $attribute_name) {
        $data = json_decode($request->getContent(), true);
        
        
        
        if (!$data) {
            throw new NotFoundHttpException("Nothing to update");
        }
        
        $profiler = $this->get('madforit.profiler');
        
        try {
            return $profiler->updateProfileAttribute(strtolower($key), $attribute_name, $data);
        } catch (\Exception $e) {
            throw new NotFoundHttpException($e->getMessage() . ' TRACE ' . $e->getTraceAsString());
        }
       

    }

}