<?php

namespace Madforit\ProfilerBundle\Controller\Api\v1;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Madforit\ProfilerBundle\Document\Profile;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/api/v1/attribute")
 */
class AttributeController extends Controller
{
    
    /**
     * @Route("/")
     * @Rest\View();
     */
    
    public function allAction()
    {
        $dm = $this->get('doctrine_mongodb')->getManager();
        
        $attributes = $dm->getRepository('MadforitProfilerBundle:Attribute')->findAll();
        
        return $attributes;
        
    }
    
    /**
     * @Route("/{key}")
     * @Rest\View();
     */
    public function getAction($key)
    {
        $dm = $this->get('doctrine_mongodb')->getManager();
        
        $profile = $dm->getRepository('MadforitProfilerBundle:Profile')->findOneBy(array('key' => $key));
        
        if (!$profile) {
            throw new NotFoundHttpException('Profile not found');
        }
        
        return $profile;
    }

}