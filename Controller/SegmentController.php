<?php

namespace Madforit\ProfilerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use Madforit\ProfilerBundle\Document\Segment;
use Madforit\ProfilerBundle\Form\SegmentType;
use Madforit\ProfilerBundle\Document\Criterion;

/**
 * @Route("/segment")
 */
class SegmentController extends Controller
{
    /**
     * @Route("/")
     * @Template()
     * @Method({"GET"})
     */
    public function indexAction()
    {
        return array();
    }
    

    
        /**
     * @Route("/_list")
     * @Method({"GET"})
     */
    public function listAction(Request $request)
    {
        $params = $request->query->all();
        
        
        $dm = $this->get('doctrine_mongodb')->getManager();
        
        $output['iTotalRecords'] = $dm->createQueryBuilder('MadforitProfilerBundle:Segment')
                ->count()
                ->getQuery()
                ->execute();
        
        $output['sEcho'] = $params['sEcho'];
        $output['iTotalDisplayRecords'] = $output['iTotalRecords'];
        
        $data = $dm->createQueryBuilder('MadforitProfilerBundle:Segment')
                ->skip($params['iDisplayStart'])
                ->limit($params['iDisplayLength'])
                ->getQuery()
                ->execute();
        
        foreach ($data as $row) {
            $output['aaData'][] = array(
                $row->getId(),
                $row->getName(),
                $row->getCriteria()->count(),
                $row->getCount(),
                $row->getCreated()->format('Y-m-d H:i:s'),
                $row->getUpdated()->format('Y-m-d H:i:s')  
            );
                    
        }
        
        return new JsonResponse($output);
        
    }
    
    /**
     * @Route("/add")
     * @Template()
     */
    public function addAction(Request $request)
    {
        $segment = new Segment();
        
        $dm = $this->get('doctrine_mongodb')->getManager();
        
        $attribute = $dm->getRepository('MadforitProfilerBundle:Attribute')->findOneByName('BingoSitePreference');
        
        $criterion = new Criterion;
        $criterion->setAttribute($attribute);
        $segment->addCriterion($criterion);
        
        $form = $this->createForm(new SegmentType(), $segment);
        
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            
            if ($form->isValid()) {
                $data = $form->getData();
                
                $count = $this->get('madforit.profiler')
                        ->buildSegmentQuery($segment)
                        ->count()
                        ->getQuery()
                        ->execute();
                
                $segment->setCount($count);
                
                $dm->persist($segment);
                $dm->flush();
                $session = $this->get('session');
                $session->getFlashBag()->add('success', 'Segment was created with ' . $segment->getCount() . ' matching profiles');
                return $this->redirect($this->generateUrl('madforit_profiler_segment_index'));
            }
        }
        
        return array('segment' => $segment, 'form'=>$form->createView());
    }
    
    /**
     * @Route("/edit/{segment_id}")
     * @Template()
     */
    public function editAction(Request $request, $segment_id)
    {
       
        
        $dm = $this->get('doctrine_mongodb')->getManager();
        
        $segment = $dm->getRepository('MadforitProfilerBundle:Segment')->findOneById($segment_id);
        
        
        $form = $this->createForm(new SegmentType(), $segment);
        
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            
            if ($form->isValid()) {
                
                $query = $this->get('madforit.profiler')
                        ->buildSegmentQuery($segment)
                        ->count()
                        ->getQuery();
                
                $count = $query->execute();
                
                $segment->setCount($count);
               
                $session = $this->get('session');
                $session->getFlashBag()->add('success', 'Segment was updated with ' . $segment->getCount() . ' matching profiles');
                
                $dm->flush();
                return $this->redirect($this->generateUrl('madforit_profiler_segment_index'));
            }
        }
        
        return array('segment' => $segment, 'form'=>$form->createView());
    }
    
}