<?php

namespace Madforit\ProfilerBundle\Service;

use Doctrine\ODM\MongoDB\DocumentManager;
use Madforit\ProfilerBundle\Document\Segment;
use Madforit\ProfilerBundle\Document\Profile;

use Symfony\Bridge\Monolog\Logger;


class Profiler {

    /**
     * @var DocumentManager
     */
    
    private $dm;
    
    /**
     * @var Logger
     */
    
    private $logger;
    
    /**
     * 
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     * @param \Symfony\Bridge\Monolog\Logger $loggger
     */
    public function __construct(DocumentManager $dm, Logger $logger) {
        $this->dm = $dm;
        $this->logger = $logger;
    }
    
    
    
    
    public function buildSegmentQuery(Segment $segment) {
        
        $hint = null;
       
        $qb = $this->dm->createQueryBuilder('MadforitProfilerBundle:Profile');
        
        foreach ($segment->getCriteria() as $criterion) {
            
            $attribute = $qb->expr()->field('attributes.attribute.$id')->equals(new \MongoId($criterion->getAttribute()->getId()));
            
            
            switch ($criterion->getAttribute()->getType()) {
                case 'Hash':
                    $field = 'attributes.data.' . $criterion->getName();
                    break;
                case 'Date':
                    $field = 'attributes.date';
                    $hint = 'date';
                    break;
                case 'Increment':
                    $field = 'attributes.increment';
                    break;
            }
                    
            
            switch ($criterion->getRule()) {
                case 'eq':
                    $function = 'equals';
                    $params = $this->parseParam($criterion->getValue(), $hint);
                    break;
                case 'neq':
                    $function = 'notEqual';
                    $params = $this->parseParam($criterion->getValue(), $hint);
                    break;
                case 'in':
                    $function = 'in';
                    $params = array();
                    
                    foreach (explode(',',$criterion->getValue()) as $param) {
                        $params[] = $this->parseParam($param, $hint);
                    }
                    break;;
                case 'notin':
                    $function = 'notIn';
                    $params = array();
                    
                    foreach (explode(',',$criterion->getValue()) as $param) {
                        $params[] = $this->parseParam($param, $hint);
                    }
                    break;;
                case 'lt':
                case 'gt':
                case 'lte':
                case 'gte':
                    $function = $criterion->getRule();
                    $params = $this->parseParam($criterion->getValue(), $hint);
                    break;;
            }
            
                
            
            $qb->addAnd($attribute);
            $qb->addAnd($qb->expr()->field($field)->$function($params));
        }
        
        
        
        return $qb;

    }
    
    private function parseParam($param, $hint = null) {
        
        if ($hint == 'date') {
            $date = new \DateTime($param);
            return $date;
        } elseif ($param == 'true') {
            return (bool) true;
        } elseif ($param == 'false') {
            return (bool) false;
        } elseif (is_numeric($param)) {
            if (preg_match('/\./', $param)) {
                return (double) $param;
            } else {
                return (int) $param;
            }
        } else {
            return (string) $param;
        }
        
    }
    
    public function findProfile($key) {
        
        return $this->dm->getRepository('MadforitProfilerBundle:Profile')->findOneBy(array('key' => $key));
        
    }
    
    public function updateProfileAttribute($key, $attribute_name, $data) {
        
       
        $profile = $this->findProfile($key);
             
        if (!$profile) {
            
            $profile = new Profile();
            $profile->setKey($key);
            $this->dm->persist($profile);
            
        }
        
        

        /** 
         * Iterate through the attributes to see if there is an attribute that
         * matches.
         * 
         */
        
        foreach ($profile->getAttributes() as $attribute) {
            
            if ($attribute->getAttribute()->getName() == $attribute_name) {
                $matching_attribute = $attribute;
                break;
            }
        }
        
        if (!isset($matching_attribute)) {
            $attribute = $this->dm->getRepository('MadforitProfilerBundle:Attribute')->findOneByName($attribute_name);
            if (!$attribute) {
                throw new \Exception('Invalid Attribute Name: ' . $attribute_name);
            }
            
            $class = '\Madforit\ProfilerBundle\Document\Attribute\\' . $attribute->getType();
            $matching_attribute = new $class;

            $matching_attribute->setAttribute($attribute);
            $profile->addAttribute($matching_attribute);

        }
        
        $this->logger->addWarning('Type is ' . $matching_attribute->getAttribute()->getType());
     
        switch ($matching_attribute->getAttribute()->getType()) {
            case 'Hash':
                if ($matching_attribute->getData()) { 
                    $change = $data + $matching_attribute->getData();
                } else {
                    $change = $data;
                }
               
                $matching_attribute->setData($change);
                
                $return = $change;
                
                break;
                
            case 'Date':
               $this->logger->addWarning('In Date Switch');
               $this->logger->addWarning('data is ' . var_export($data, true));
                
                
               if (is_array($data) && isset($data['ISO8601'])) {
                   $matching_attribute->setDate(new \DateTime($data['ISO8601']));
               }
                
               $return = $date;
               break;
               
        }
            
        
        
        $this->dm->flush();
        
        unset($profile);
        
        return $return;
    }
    
}

?>
